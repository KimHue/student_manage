import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    /*MENU*/
    public static void menu() {
        System.out.print("-----------------------MENU---------------------------\n" +
                "1. Lấy tất cả dữ liệu trong bảng\n" +
                "2. Lấy danh sách những bạn học sinh nhỏ tuổi nhất\n" +
                "3. Lấy danh sách những bạn học sinh lớn tuổi nhất\n" +
                "4. Thêm 1 list Sinh viên\n");
        System.out.print("Chọn menu: ");

        Scanner scanner = new Scanner(System.in);
        int choose = scanner.nextInt();
        switch (choose) {
            case 1:
                List<Student> studentList = new StudentDAO().getStudents();
                for (Student st : studentList) {
                    st.print();
                }
                break;
            case 2:
                List<Student> youngestStudents = new StudentDAO().getYoungestStudents();
                for (Student st : youngestStudents) {
                    st.print();
                }
                break;
            case 3:
                List<Student> oldestStudents = new StudentDAO().getOldestStudents();
                for (Student sv : oldestStudents) {
                    sv.print();
                }
                break;
            case 4:
                Scanner scan = new Scanner(System.in);
                System.out.print("Nhap so luong sinh vien: ");
                int n = scan.nextInt();
                for (int i = 0; i < n; i++) {
                    Student student = new Student();
                    student.input();
                    new StudentDAO().insertStudents(student);
                }
                break;
            default:
                System.out.println("Không hợp lệ!!!");
        }

    }

    public static void main(String[] args) {
        menu();
    }
}
