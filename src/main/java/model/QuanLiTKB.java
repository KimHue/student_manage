package model;

public class QuanLiTKB {
    private int monhoc_id;
    private int giaovien_id;
    private int hocKi;

    public QuanLiTKB(){}

    public int getMonhoc_id() {
        return monhoc_id;
    }

    public void setMonhoc_id(int monhoc_id) {
        this.monhoc_id = monhoc_id;
    }

    public int getGiaovien_id() {
        return giaovien_id;
    }

    public void setGiaovien_id(int giaovien_id) {
        this.giaovien_id = giaovien_id;
    }

    public int getHocKi() {
        return hocKi;
    }

    public void setHocKi(int hocKi) {
        this.hocKi = hocKi;
    }
}
