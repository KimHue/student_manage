package model;

import java.util.Scanner;

public class MonHoc {
    private int id;
    private String tenMonHoc;
    private int troGiangId;
    private int khoa_id;

    public MonHoc() {
    }

    public MonHoc(int id, String tenMonHoc, int troGiangId, int khoa_id) {
        this.id = id;
        this.tenMonHoc = tenMonHoc;
        this.troGiangId = troGiangId;
        this.khoa_id = khoa_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenMonHoc() {
        return tenMonHoc;
    }

    public void setTenMonHoc(String tenMonHoc) {
        this.tenMonHoc = tenMonHoc;
    }

    public int getTroGiangId() {
        return troGiangId;
    }

    public void setTroGiangId(int troGiangId) {
        this.troGiangId = troGiangId;
    }

    public int getKhoa_id() {
        return khoa_id;
    }

    public void setKhoa_id(int khoa_id) {
        this.khoa_id = khoa_id;
    }

    public void print() {
        System.out.println("id: " + getId());
        System.out.println("tên môn học: " + getTenMonHoc());
        System.out.println("mã trợ giảng: " + getTroGiangId());
        System.out.println("mã Khóa học: " + getKhoa_id());
    }

    public void input() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhập tên môn học: ");
        setTenMonHoc(scan.nextLine());
        System.out.print("Nhập mã trợ giảng: ");
        setTroGiangId(scan.nextInt());
        scan.nextLine();
        System.out.println("Nhập mã khóa học: ");
        setKhoa_id(scan.nextInt());
    }
}
