package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class SinhVien {
    private int id;
    private String hoTen;
    private Date ngaySinh;
    private String diaChi;

    //constructor


    public SinhVien(int id, String hoTen, Date ngaySinh, String diaChi) {
        this.id = id;
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
        this.diaChi = diaChi;
    }

    public SinhVien(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public void print() {
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        System.out.println("id: " + id);
        System.out.println("họ tên: " + hoTen);
        System.out.println("ngày sinh: " + date.format(ngaySinh));
        System.out.println("địa chỉ: " + diaChi);
        System.out.println("-------------------------------------------");
    }

    public void input() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhap id = ");
        setId(scan.nextInt());
        scan.nextLine();
        System.out.print("Nhập tên sinh viên: ");
        setHoTen(scan.nextLine());

        //ngaySinh
        System.out.print("Nhập ngày sinh: ");
        String date = scan.nextLine();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date1 = null;
        try {
            date1 = dateFormat.parse(date);
            setNgaySinh(date1);
        } catch (ParseException ex) {
            System.out.println("Lỗi đăng nhập ngày sinh!");
        }
        System.out.print("Nhập địa chỉ: ");
        this.diaChi = scan.nextLine();
    }
}

