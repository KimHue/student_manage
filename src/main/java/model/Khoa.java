package model;

import java.util.Scanner;

public class Khoa {
    private int id;
    private String tenKhoa;

    public Khoa(int id, String tenKhoa) {
        this.id = id;
        this.tenKhoa = tenKhoa;
    }

    public Khoa(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenKhoa() {
        return tenKhoa;
    }

    public void setTenKhoa(String tenKhoa) {
        this.tenKhoa = tenKhoa;
    }

    public void print(){
        System.out.print("id: "+id);
        System.out.print("tên khóa học: "+tenKhoa);
    }

    public void input(){
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhap id = ");
        setId(scan.nextInt());
        scan.nextLine();
        System.out.print("Nhập tên khóa học: ");
        this.tenKhoa=scan.nextLine();
    }
}
