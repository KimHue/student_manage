package model;

public class QuanLiDiem {
    private int sinhvien_id;
    private int monhoc_id;
    private float diem;

    public QuanLiDiem(){

    }

    public int getSinhvien_id() {
        return sinhvien_id;
    }

    public void setSinhvien_id(int sinhvien_id) {
        this.sinhvien_id = sinhvien_id;
    }

    public int getMonhoc_id() {
        return monhoc_id;
    }

    public void setMonhoc_id(int monhoc_id) {
        this.monhoc_id = monhoc_id;
    }

    public float getDiem() {
        return diem;
    }

    public void setDiem(float diem) {
        this.diem = diem;
    }
}
