import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQLConnect {
    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String SERVER_HOST = "localhost:3306";
    private static final String DB_NAME = "danhsachsv";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "GrayBoshiHK1998";


    private static void initializeClass(String className) throws ClassNotFoundException {
        Class.forName(className);
    }


    /**
     * @method: get connection
     * @return connection
     */
    public static Connection getConnection() {
        Connection connect = null;
        try {
            initializeClass(DRIVER_NAME);
            connect = DriverManager.getConnection("jdbc:mysql://" + SERVER_HOST + "/" + DB_NAME +
                    "?user=" + USERNAME + "&password=" + PASSWORD);

        } catch (Exception e) {
            System.out.println("Can not connect to database");
        }
        return connect;
    }


    /**
     * @method: close
     * @param con: Connection Interface
     * @param preparedStatement: PreparedStatement Interface => (parameter query)
     */

    public static void close(Connection con, PreparedStatement preparedStatement){
        try{
            if(con!=null){
                con.close();
            }
            if (preparedStatement!=null){
                preparedStatement.close();
            }
        }catch (SQLException ex){
            System.out.println("Error");
        }
    }
}
