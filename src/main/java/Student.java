import java.util.Scanner;

public class Student {
    // create properties
    private int id;
    private String name;
    private int age;


    //create method get & set
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void print(){
        System.out.println("id: "+id);
        System.out.println("name: "+name);
        System.out.println("age: "+age);
        System.out.println("-------------------------------------------");
    }

    public void input(){
        Scanner scan = new Scanner(System.in);
        System.out.print("Studen name: ");
        this.name = scan.nextLine();
        System.out.print("Student age: ");
        this.age = scan.nextInt();
    }

}
