package dbconnect;

import java.sql.*;

public class DBConnection {
    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String SERVER_HOST = "localhost:3306";
    private static final String DB_NAME = "quanlisinhvien";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "GrayBoshiHK1998";

    private static DBConnection dbConnection;

    private DBConnection() {
    }

    public static DBConnection getInstance(){
        if (dbConnection == null){
            dbConnection = new DBConnection();
        }
        return dbConnection;
    }

    public Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName(DRIVER_NAME);
            conn = DriverManager.getConnection("jdbc:mysql://" + SERVER_HOST + "/" + DB_NAME +
                    "?user=" + USERNAME + "&password=" + PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public void close(Connection con, PreparedStatement preparedStatement){
        try{
            if(con!=null){
                con.close();
            }
            if (preparedStatement!=null){
                preparedStatement.close();
            }
        }catch (SQLException ex){
            System.out.println("Error");
        }
    }

    public static void main(String[] args) {

        Connection conn = DBConnection.getInstance().getConnection();
        try {
            String query = "SELECT * FROM quanlisinhvien.sinhvien;";
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1) + " " + resultSet.getString(2));
            }
        } catch (Exception e) {
            System.out.println("Can not connect to database");
        }
    }
}
