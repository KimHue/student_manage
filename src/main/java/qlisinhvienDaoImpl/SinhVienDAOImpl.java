package qlisinhvienDaoImpl;

import dbconnect.DBConnection;
import model.SinhVien;
import quanlisinhviendao.SinhVienDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class SinhVienDAOImpl implements SinhVienDAO {
    //Các câu truy vấn sql
    private static final String GET_LIST_HS = "SELECT * FROM sinhvien;";
    private static final String GET_BY_ID = "SELECT * FROM sinhvien WHERE ID = ?;";
    private static final String ADD_HS = "INSERT INTO sinhvien (hoTen, ngaySinh, diaChi) VALUES (?,?,?);";
    private static final String UPDATE_HS = "UPDATE sinhvien SET hoTen = ?,ngaySinh = ?,diaChi = ? WHERE id = ?;";
    private static final String DELETE_HS = "DELETE FROM sinhvien WHERE id = ?;";
    private static final String DELETE_TROGIANG = "DELETE FROM monhoc WHERE troGiangId = ?;";

    //Khởi tạo đối tượng db
    private DBConnection db = DBConnection.getInstance();

    @Override
    /*lấy danh sách các học sinh
     *trả về ds các học sinh
     * */
    public List<SinhVien> getHocSinhs() {
        List<SinhVien> sinhVienList = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        try {
            //B1.Tạo kết nối tới CSDL
            con = db.getConnection();

            //B2.Viết câu lệnh truy vấn
            String sql = GET_LIST_HS;

            //B3.Thực hiện chạy lệnh sql bằng Statement
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                SinhVien sinhVien = new SinhVien(rs.getInt("id"), rs.getString("hoTen"),
                        rs.getDate("ngaySinh"), rs.getString("diaChi"));
                sinhVienList.add(sinhVien);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            //đóng chương trình
            db.close(con, ps);
        }
        return sinhVienList;
    }


    @Override
    /*
     * Lấy đối tượng Sinh Vien qua id
     * Trả về 1 đối tương  Sinh Vien
     * */
    public SinhVien getHocSinhId(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = GET_BY_ID;
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                SinhVien sinhVien = new SinhVien(rs.getInt("id"), rs.getString("hoTen"),
                        rs.getDate("ngaySinh"), rs.getString("diaChi"));
                return sinhVien;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return null;
    }

    @Override
    /*
     *Thêm 1 Hoc Vien vào bảng
     * */
    public boolean addHocSinh(SinhVien sinhVien) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = ADD_HS;
            ps = con.prepareStatement(sql);
            ps.setString(1, sinhVien.getHoTen());
            ps.setDate(2, new Date(sinhVien.getNgaySinh().getTime()));
            ps.setString(3, sinhVien.getDiaChi());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return true;
    }

    @Override
    /*
     * Sửa Hoc Vien
     * */
    public boolean updateHocSinh(SinhVien sinhVien) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = UPDATE_HS;
            ps = con.prepareStatement(sql);
            ps.setInt(4, sinhVien.getId());
            ps.setString(1, sinhVien.getHoTen());
            ps.setDate(2, new Date(sinhVien.getNgaySinh().getTime()));
            ps.setString(3, sinhVien.getDiaChi());
            int result = ps.executeUpdate();
            System.out.println(result);
            if (result == 0) {
                System.out.println("id khong ton tai");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }

        return true;
    }

    @Override
    /*
     *Xoa 1 hoc vien trong bảng
     * */
    public boolean deleteHocSinh(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        try {
            con = db.getConnection();
            ps = con.prepareStatement(DELETE_HS);
            ps1 = con.prepareStatement(DELETE_TROGIANG);

            ps.setInt(1, id);
            int kq = ps.executeUpdate();
            if (kq == 1) {
                ps1.setInt(1, id);
                ps1.executeUpdate();
                System.out.println("Ok");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return true;
    }
}
