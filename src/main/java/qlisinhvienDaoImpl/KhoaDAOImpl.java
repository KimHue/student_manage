package qlisinhvienDaoImpl;

import dbconnect.DBConnection;
import model.GiaoVien;
import model.Khoa;
import model.MonHoc;
import quanlisinhviendao.KhoaDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class KhoaDAOImpl implements KhoaDAO {

    //Các câu truy vấn sql
    private static final String GET_LIST_KHOA = "SELECT * FROM khoa;";
    private static final String GET_BY_ID = "SELECT * FROM khoa WHERE ID = ?;";
    private static final String ADD_KHOA = "INSERT INTO khoa (id,tenKhoa) VALUES (?,?);";
    private static final String UPDATE_KHOA = "UPDATE khoa SET tenKhoa = ? WHERE id = ?;";
    private static final String DELETE_KHOA = "DELETE FROM khoa WHERE id = ?;";
    private static final String DELETE_MONHOC = "DELETE FROM monhoc WHERE khoa_id = ?;";

    //Khởi tạo đối tượng db
    private DBConnection db = DBConnection.getInstance();

    @Override
    public List<Khoa> getDSKhoa() {
        Connection con = null;
        PreparedStatement ps = null;
        List<Khoa> khoas = new ArrayList<>();
        try {
            //Tạo kết nối tới CSDL
            con = db.getConnection();

            //Câu lệnh truy vấn
            String sql = GET_LIST_KHOA;

            //Thục hiện chạy lệnh sql bằng Statement
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Khoa khoa = new Khoa(rs.getInt("id"),rs.getString("tenKhoa"));

                khoas.add(khoa);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            //đóng chương trình
            db.close(con, ps);
        }
        return khoas;
    }

    @Override
    public Khoa getKhoaId(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            //Tạo kết nối tới CSDL
            con = db.getConnection();

            //Câu lệnh truy vấn
            String sql = GET_LIST_KHOA;

            //Thục hiện chạy lệnh sql bằng Statement
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Khoa khoa = new Khoa(rs.getInt("id"),rs.getString("tenKhoa"));

                return khoa;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            //đóng chương trình
            db.close(con, ps);
        }
        return null;
    }

    @Override
    public boolean addKhoa(Khoa khoa) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = ADD_KHOA;
            ps = con.prepareStatement(sql);
            ps.setInt(1, khoa.getId());
            ps.setString(2, khoa.getTenKhoa());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }

    @Override
    public boolean updateKhoa(Khoa khoa) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = UPDATE_KHOA;
            ps = con.prepareStatement(sql);
            ps.setInt(1, khoa.getId());
            ps.setString(2, khoa.getTenKhoa());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }

    @Override
    public boolean deleteKhoa(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        try {
            con = db.getConnection();
            String sql = DELETE_KHOA;
            String sql1 = DELETE_MONHOC;
            ps = con.prepareStatement(sql);
            ps1 = con.prepareStatement(sql1);
            ps.setInt(1, id);
            // ps.addBatch();
            int kq = ps.executeUpdate();
            if (kq == 1) {
                ps1.setInt(1, id);
                ps1.executeUpdate();
                System.out.println("Ok");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }
}
