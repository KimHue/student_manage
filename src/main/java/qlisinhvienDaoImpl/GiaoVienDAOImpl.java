package qlisinhvienDaoImpl;

import dbconnect.DBConnection;
import model.GiaoVien;
import quanlisinhviendao.GiaoVienDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GiaoVienDAOImpl implements GiaoVienDAO {
    //Các câu truy vấn sql
    private static final String GET_LIST_GV = "SELECT * FROM giaovien;";
    private static final String GET_BY_ID = "SELECT * FROM giaovien WHERE ID = ?;";
    private static final String ADD_GV = "INSERT INTO giaovien (hoTen, ngaySinh, diaChi) VALUES (?,?,?);";
    private static final String UPDATE_GV = "UPDATE giaovien SET hoTen = ?,ngaySinh = ?,diaChi = ? WHERE id = ?;";
    private static final String DELETE_GV = "DELETE FROM giaovien WHERE id = ?;";


    //Khởi tạo đối tượng db
    private DBConnection db = DBConnection.getInstance();

    @Override
    /*
     * Lấy danh sách giáo viên
     * Trả về 1 danh sách gv
     * */
    public List<GiaoVien> getDSGiaoVien() {
        Connection con = null;
        PreparedStatement ps = null;
        List<GiaoVien> giaoViens = new ArrayList<>();
        try {
            //Tạo kết nối tới CSDL
            con = db.getConnection();

            //Câu lệnh truy vấn
            String sql = GET_LIST_GV;

            //Thục hiện chạy lệnh sql bằng Statement
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                GiaoVien gv = new GiaoVien(rs.getInt("id"), rs.getString("hoTen"),
                        rs.getDate("ngaySinh"), rs.getString("diaChi"));
                giaoViens.add(gv);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            //đóng chương trình
            db.close(con, ps);
        }
        return giaoViens;
    }

    @Override
    /*
     * Lấy giáo viên theo id
     * Trả về 1 gv
     * */
    public GiaoVien getGiaoVienId(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = GET_BY_ID;
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                GiaoVien gv = new GiaoVien(rs.getInt("id"), rs.getString("hoTen"),
                        rs.getDate("ngaySinh"), rs.getString("diaChi"));
                return gv;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return null;
    }

    /*
     * Thêm giáo viên vào bảng
     * */
    @Override
    public boolean addGiaoVien(GiaoVien giaoVien) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = ADD_GV;
            ps = con.prepareStatement(sql);
            ps.setString(1, giaoVien.getHoTen());
            ps.setDate(2, new Date(giaoVien.getNgaySinh().getTime()));
            ps.setString(3, giaoVien.getDiaChi());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }

    /*
     * Sửa GV
     * */
    @Override
    public boolean updateGiaoVien(GiaoVien giaoVien) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = UPDATE_GV;
            ps = con.prepareStatement(sql);
            ps.setInt(4, giaoVien.getId());
            ps.setString(1, giaoVien.getHoTen());
            ps.setDate(2, new Date(giaoVien.getNgaySinh().getTime()));
            ps.setString(3, giaoVien.getDiaChi());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }

    /*
     * Xóa GV
     * */
    @Override
    public boolean deleteGiaoVien(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            String sql = DELETE_GV;
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }
}
