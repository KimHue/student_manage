package qlisinhvienDaoImpl;

import dbconnect.DBConnection;
import model.MonHoc;
import quanlisinhviendao.MonHocDAO;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MonHocDAOImpl implements MonHocDAO {

    private static final String GET_LIST_MONHOC = "SELECT * FROM monhoc;";
    private static final String GET_BY_ID = "SELECT * FROM monhoc WHERE ID = ?;";
    private static final String ADD_MONHOC = "INSERT INTO monhoc (id,tenMonHoc,troGiangId,khoa_id) VALUES (?,?,?,?);";
    private static final String UPDATE_MONHOC = "UPDATE monhoc SET tenMonHoc = ?, troGiang_id = ?, khoa_id =? WHERE id = ?;";
    private static final String DELETE_MONHOC = "DELETE FROM monhoc WHERE id = ?;";


    DBConnection db = DBConnection.getInstance();

    @Override
    public List<MonHoc> getDSMonHoc() {
        List<MonHoc> monHocList = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            ps = con.prepareStatement(GET_LIST_MONHOC);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                MonHoc monHoc = new MonHoc(rs.getInt("id"), rs.getString("tenMonHoc"),
                        rs.getInt("troGiangId"), rs.getInt("khoa_id"));
                monHocList.add(monHoc);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        return monHocList;
    }

    @Override
    public MonHoc getMonHocId(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            ps = con.prepareStatement(GET_BY_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                MonHoc monHoc = new MonHoc(rs.getInt("id"), rs.getString("tenMonHoc"),
                        rs.getInt("troGiangId"), rs.getInt("khoa_id"));
                return monHoc;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return null;
    }

    @Override
    public boolean addMonHoc(MonHoc monHoc) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            ps = con.prepareStatement(ADD_MONHOC);
            ps.setInt(1, monHoc.getId());
            ps.setString(2, monHoc.getTenMonHoc());
            ps.setInt(3, monHoc.getTroGiangId());
            ps.setInt(4, monHoc.getKhoa_id());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }

    @Override
    public boolean updateMonHoc(MonHoc monHoc) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            ps = con.prepareStatement(UPDATE_MONHOC);
            ps.setInt(1, monHoc.getId());
            ps.setString(2, monHoc.getTenMonHoc());
            ps.setInt(3, monHoc.getTroGiangId());
            ps.setInt(4, monHoc.getKhoa_id());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }

    @Override
    public boolean deleteMonHoc(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = db.getConnection();
            ps = con.prepareStatement(DELETE_MONHOC);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            db.close(con, ps);
        }
        return false;
    }
}
