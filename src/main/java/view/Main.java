package view;

import model.SinhVien;
import qlisinhvienDaoImpl.SinhVienDAOImpl;
import quanlisinhviendao.SinhVienDAO;

import java.util.List;
import java.util.Scanner;

public class Main {
    //Hàm nhập
    static int nhap() {
        Scanner scan = new Scanner(System.in);
        while (true) {
            try {
                System.out.print("Chọn số: ");
                int n = scan.nextInt();
                return n;
            } catch (Exception ex) {
                String bad_input = scan.next();
                System.out.println("Lỗi: " + bad_input + " không phải là số");
                continue;
            }
        }
    }

    //Hàm menuDetail
    static void menuDetail() {
        System.out.println("-----------------Detail Menu----------------");
        System.out.println("1.Hiển thị danh sách");
        System.out.println("2.Tìm kiếm theo id");
        System.out.println("3.Thêm");
        System.out.println("4.Sửa");
        System.out.println("5.Xóa");
        System.out.println("----------------------------------------------");
    }

    //Menu
    static void menu() {
        System.out.println("-----------------------------------MENU-----------------------------------");
        System.out.println("1.Học Viên");
        System.out.println("2.Giáo Viên");
        System.out.println("3.Khóa Học");
        System.out.println("4.Môn Học");
        System.out.println("5.Thoát");
        System.out.println("---------------------------------------------------------------------------");
    }

    //xuLi
    static void xuLiMenu() {
        menu();
        int chon = nhap();
        switch (chon) {

            case 1:
                int num1;
                do {
                    menuDetail();
                    View view = new View();
                    num1 = nhap();
                    view.thongTinSinhVien(num1);
                } while (num1 < 5 && num1 > 0);
                break;

            case 2:
                int num2;
                do {
                    menuDetail();
                    View view1 = new View();
                    num2 = nhap();
                    view1.thongTinGV(num2);
                } while (num2 < 5 && num2 > 0);
                break;

            case 3:
                int num3;
                do {
                    menuDetail();
                    View view2 = new View();
                    num3 = nhap();
                    view2.thongTinKhoaHoc(num3);
                } while (num3 < 5 && num3 > 0);
                break;

            case 4:
                int num4;
                do {
                    menuDetail();
                    View view3 = new View();
                    num4 = nhap();
                    view3.thongTinMonHoc(num4);
                } while (num4 < 5 && num4 > 0);
                break;
            default:
                break;
        }

    }

    public static void main(String[] args) {
        xuLiMenu();
    }
}
