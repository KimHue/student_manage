package view;

import model.GiaoVien;
import model.Khoa;
import model.MonHoc;
import model.SinhVien;
import qlisinhvienDaoImpl.GiaoVienDAOImpl;
import qlisinhvienDaoImpl.KhoaDAOImpl;
import qlisinhvienDaoImpl.MonHocDAOImpl;
import qlisinhvienDaoImpl.SinhVienDAOImpl;

import java.awt.*;
import java.util.List;
import java.util.Scanner;

public class View {
    //học sinh
    public void thongTinSinhVien(int chon) {

            switch (chon) {
                case 1:
                    //lấy danh sách sinh viên
                    SinhVienDAOImpl sinhVienDAOImpl = new SinhVienDAOImpl();
                    List<SinhVien> sinhVienList = sinhVienDAOImpl.getHocSinhs();
                    for (SinhVien sinhVien : sinhVienList) {
                        sinhVien.print();
                    }
                    break;
                case 2:
                    //lay theo id
                    Scanner scanner = new Scanner(System.in);
                    System.out.print("nhap id = ");
                    int n = scanner.nextInt();
                    SinhVienDAOImpl sinhVienDAOImpl1 = new SinhVienDAOImpl();
                    SinhVien sinhVien = sinhVienDAOImpl1.getHocSinhId(n);
                    if (sinhVien != null) {
                        sinhVien.print();
                    } else {
                        System.out.println("không tồn tại id này!");
                    }
                    break;
                case 3:
                    //add sinh vien
                    SinhVien sv = new SinhVien();
                    sv.input();
                    new SinhVienDAOImpl().addHocSinh(sv);
                    break;
                case 4:
                    //update sinh vien
                    SinhVien sinhVien1 = new SinhVien();
                    sinhVien1.input();
                    new SinhVienDAOImpl().updateHocSinh(sinhVien1);
                    break;
                case 5:
                    //delete sinh vien
                    Scanner sc = new Scanner(System.in);
                    System.out.print("Nhập id cần xóa: ");
                    int num = sc.nextInt();
                    new SinhVienDAOImpl().deleteHocSinh(num);
                    break;
                default:
                    System.out.println("Không hợp lệ!");
                    break;
            }


    }

    //Giáo viên
    public void thongTinGV(int chon) {

            switch (chon) {
                case 1:
                    //lấy danh sách gv
                    GiaoVienDAOImpl giaoVienDAOImpl = new GiaoVienDAOImpl();
                    List<GiaoVien> gvList = giaoVienDAOImpl.getDSGiaoVien();
                    for (GiaoVien gv : gvList) {
                        gv.print();
                    }
                    break;
                case 2:
                    //lay theo id
                    Scanner scanner = new Scanner(System.in);
                    System.out.print("nhap id = ");
                    int n = scanner.nextInt();
                    GiaoVienDAOImpl giaoVienDAOImpl1 = new GiaoVienDAOImpl();
                    GiaoVien giaoVien = giaoVienDAOImpl1.getGiaoVienId(n);
                    if (giaoVien != null) {
                        giaoVien.print();
                    } else {
                        System.out.println("không tồn tại id này!");
                    }
                    break;
                case 3:
                    //add giáo viên
                    GiaoVien gv = new GiaoVien();
                    gv.input();
                    new GiaoVienDAOImpl().addGiaoVien(gv);
                    break;
                case 4:
                    //update  viensinh
                    GiaoVien gv1 = new GiaoVien();
                    gv1.input();
                    new GiaoVienDAOImpl().updateGiaoVien(gv1);
                    break;
                case 5:
                    //delete viensinh
                    Scanner sc = new Scanner(System.in);
                    System.out.print("Nhập id cần xóa: ");
                    int num = sc.nextInt();
                    new GiaoVienDAOImpl().deleteGiaoVien(num);
                    break;
                default:
                    System.out.println("Không hợp lệ!");
                    break;

        }
    }

    //khoa học
    public void thongTinKhoaHoc(int chon) {

        switch (chon) {
            case 1:
                //lấy danh sách khóa học
                KhoaDAOImpl khoaDAOImpl = new KhoaDAOImpl();
                List<Khoa> khoaList = khoaDAOImpl.getDSKhoa();
                for (Khoa khoa : khoaList) {
                    khoa.print();
                }
                break;
            case 2:
                //lay theo id
                Scanner scanner = new Scanner(System.in);
                System.out.print("nhap id = ");
                int n = scanner.nextInt();
                KhoaDAOImpl khoaDAOImpl1 = new KhoaDAOImpl();
                Khoa khoa = khoaDAOImpl1.getKhoaId(n);
                if (khoa != null) {
                    khoa.print();
                } else {
                    System.out.println("không tồn tại id này!");
                }
                break;
            case 3:
                //add khóa học
                Khoa khoa1 = new Khoa();
                khoa1.input();
                new KhoaDAOImpl().addKhoa(khoa1);
                break;
            case 4:
                //update khóa học
                Khoa k = new Khoa();
                k.input();
                new KhoaDAOImpl().updateKhoa(k);
                break;
            case 5:
                //delete khóa
                Scanner sc = new Scanner(System.in);
                System.out.print("Nhập id cần xóa: ");
                int num = sc.nextInt();
                new KhoaDAOImpl().deleteKhoa(num);
                break;
            default:
                System.out.println("Không hợp lệ!");
                break;
        }

    }


    //môn học
    public void thongTinMonHoc(int chon) {

        switch (chon) {
            case 1:
                //lấy danh sách môn học
                MonHocDAOImpl monHocDAOImpl = new MonHocDAOImpl();
                List<MonHoc> monHocList = monHocDAOImpl.getDSMonHoc();
                for (MonHoc monHoc : monHocList) {
                    monHoc.print();
                }
                break;
            case 2:
                //lay theo id
                Scanner scanner = new Scanner(System.in);
                System.out.print("nhap id = ");
                int n = scanner.nextInt();
                MonHocDAOImpl monHocDAOImpl1 = new MonHocDAOImpl();

                MonHoc monHoc = monHocDAOImpl1.getMonHocId(n);
                if (monHoc != null) {
                    monHoc.print();
                } else {
                    System.out.println("không tồn tại id này!");
                }
                break;
            case 3:
                //add môn học
                MonHoc monHoc1 = new MonHoc();
                monHoc1.input();
                new MonHocDAOImpl().addMonHoc(monHoc1);
                break;
            case 4:
                //update môn học
                MonHoc monHoc2 = new MonHoc();
                monHoc2.input();
                new MonHocDAOImpl().updateMonHoc(monHoc2);
                break;
            case 5:
                //delete môn học
                Scanner sc = new Scanner(System.in);
                System.out.print("Nhập id cần xóa: ");
                int num = sc.nextInt();
                new MonHocDAOImpl().deleteMonHoc(num);
                break;
            default:
                System.out.println("Không hợp lệ");
                break;
        }

    }
}
