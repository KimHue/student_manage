package quanlisinhviendao;

import model.GiaoVien;

import java.util.List;

public interface GiaoVienDAO {
    List<GiaoVien> getDSGiaoVien();

    GiaoVien getGiaoVienId(int id);

    boolean addGiaoVien(GiaoVien giaoVien);

    boolean updateGiaoVien(GiaoVien giaoVien);

    boolean deleteGiaoVien(int id);
}
