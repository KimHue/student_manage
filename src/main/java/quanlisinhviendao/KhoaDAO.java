package quanlisinhviendao;

import model.Khoa;

import java.util.List;

public interface KhoaDAO {
    List<Khoa> getDSKhoa();

    Khoa getKhoaId(int id);

    boolean addKhoa(Khoa khoa);

    boolean updateKhoa(Khoa khoa);

    boolean deleteKhoa(int id);
}
