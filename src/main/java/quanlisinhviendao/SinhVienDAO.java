package quanlisinhviendao;

import model.SinhVien;

import java.util.List;

public interface SinhVienDAO {
    List<SinhVien> getHocSinhs();

    SinhVien getHocSinhId(int id);

    boolean addHocSinh(SinhVien sinhVien);

    boolean updateHocSinh(SinhVien sinhVien);

    boolean deleteHocSinh(int id);
}
