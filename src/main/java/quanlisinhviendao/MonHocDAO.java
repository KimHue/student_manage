package quanlisinhviendao;

import model.MonHoc;

import java.util.List;

public interface MonHocDAO {
    List<MonHoc> getDSMonHoc();

    MonHoc getMonHocId(int id);

    boolean addMonHoc(MonHoc monHoc);

    boolean updateMonHoc(MonHoc monHoc);

    boolean deleteMonHoc(int id);
}
