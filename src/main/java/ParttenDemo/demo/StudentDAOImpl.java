package ParttenDemo.demo;

import java.util.ArrayList;
import java.util.List;

public class StudentDAOImpl implements StudentDAO{

    List<Student> students;
    public StudentDAOImpl(){
        students = new ArrayList<>();
        Student student1 = new Student("Nguyen Van A",0);
        Student student2 = new Student("Nguyen Van B",1);
        students.add(student1);
        students.add(student2);
    }

    @Override
    public List<Student> getStudents() {
        return students;
    }

    @Override
    public Student getStudent(int id) {
       return students.get(id);
    }

    @Override
    public void updateStudent(Student student) {
        students.get(student.getId()).setName(student.getName());
        System.out.println("Sửa thành công");
    }

    @Override
    public void deleteStudent(Student student) {
        students.remove(student.getId());
        System.out.println("Xoa thanh cong");
    }
}
