package ParttenDemo.demo;

public class Main {
    public static void main(String[] args) {
        StudentDAO studentDao = new StudentDAOImpl();

        //print all students
        for (Student student : studentDao.getStudents()) {
            System.out.println("Student: [id : " + student.getId() + ", Name : " +
                    student.getName() + " ]");
        }

        //update student
        Student student = studentDao.getStudents().get(0);
        student.setName("Hue");
        studentDao.updateStudent(student);

        //get the student
        studentDao.getStudent(0);
        System.out.println("Student: [id : " + student.getId() + ", Name : " +
                student.getName() + " ]");

        //delete

        studentDao.deleteStudent(student);

//        for (Student student1 : studentDao.getStudents()) {
//            System.out.println("Student: [id : " + student1.getId() + ", Name : " +
//                    student.getName() + " ]");
//        }
    }
}
