package ParttenDemo.demo;

import java.util.List;

public interface StudentDAO {
    public List<Student> getStudents();
    public Student getStudent(int id);
    public void updateStudent(Student student);
    public void deleteStudent(Student student);
}
