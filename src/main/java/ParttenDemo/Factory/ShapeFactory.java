package ParttenDemo.Factory;

public class ShapeFactory {
    public Shape getShape(String shapeType){
        if(shapeType == null){
            return null;
        }
//        Phương thức equalsIgnoreCase() so sánh hai chuỗi đưa ra
//        dựa trên nội dung của chuỗi không phân biệt chữ hoa và
//        chữ thường. Nếu hai chuỗi khác nhau nó trả về false.
//         Nếu hai chuỗi bằng nhau nó trả về true.

        if(shapeType.equalsIgnoreCase("hinh tron")){
            return new Circle();
        } else if(shapeType.equalsIgnoreCase("hinh chu nhat")){
            return new Rectangle();
        } else if(shapeType.equalsIgnoreCase("hinh vuong")){
            return new Square();
        }
        return null;
    }
}
