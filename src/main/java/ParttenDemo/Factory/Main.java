package ParttenDemo.Factory;

public class Main {
    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();

        Shape shape1 = shapeFactory.getShape("hinh tron");
        //goi den ham draw
        shape1.draw();


        Shape shape2 = shapeFactory.getShape("HINH vuong");
        shape2.draw();


        Shape shape3 = shapeFactory.getShape("HINH CHU NHAT");
        shape3.draw();
    }
}
