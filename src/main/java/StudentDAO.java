import dbconnect.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {
    private static final String GET_TABLE = "SELECT * FROM danhsachsv.sinhvien;";
    private static final String MIN_AGE = "SELECT id, name, age FROM danhsachsv.sinhvien WHERE age = (SELECT MIN(age) FROM danhsachsv.sinhvien);";
    private static final String MAX_AGE = "SELECT id, name, age FROM danhsachsv.sinhvien WHERE age = (SELECT MAX(age) FROM danhsachsv.sinhvien);";
    private static final String INSERT_STUDENT ="INSERT INTO sinhvien(name,age) VALUES (?,?);";

    /**
     * Get List Student
     *
     * @return studentList
     */

    private DBConnection dbConnection = DBConnection.getInstance();
    public List<Student> getStudents() {
        //create student list
        List<Student> studentList = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        try {
            //connect database
            con = dbConnection.getConnection();
            String sql = GET_TABLE;
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            //get student in Database => add studentlist
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setAge(rs.getInt("age"));
                studentList.add(student);
            }
        } catch (SQLException ex) {
            System.out.println("program error!");
        } finally {
            dbConnection.close(con, ps);
        }
        return studentList;
    }

    /**
     * get youngest student
     *
     * @return studentList
     */
    public List<Student> getYoungestStudents() {
        //create student list
        List<Student> studentList = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = dbConnection.getConnection();
            String sql = MIN_AGE;
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setAge(rs.getInt("age"));
                studentList.add(student);
            }
        } catch (SQLException ex) {
            System.out.println("program error!");
        } finally {
            dbConnection.close(con, ps);
        }
        return studentList;
    }

    /**
     * get oldest student
     *
     * @return studentList
     */
    public List<Student> getOldestStudents() {
        //create student list
        List<Student> studentList = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = dbConnection.getConnection();
            String sql = MAX_AGE;
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setAge(rs.getInt("age"));
                studentList.add(student);
            }
        } catch (SQLException ex) {
            System.out.println("program error!");
        } finally {
            dbConnection.close(con, ps);
        }
        return studentList;
    }

    /**
     * Insert student to Database
     *
     * @return student
     */

    public Student insertStudents(Student student) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DBConnection.getInstance().getConnection();
            String sql = INSERT_STUDENT;
            ps = con.prepareStatement(sql);
            ps.setString(1, student.getName());
            ps.setInt(2, student.getAge());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            DBConnection.getInstance().close(con, ps);
        }
        return student;
    }

}